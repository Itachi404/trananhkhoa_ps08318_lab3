<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//	Thêm danh mục
Route::post('/create', ['as'=>'C', 'uses'=>'UserController@create']);
//	Xem danh mục
Route::get('/', ['as'=>'R', 'uses'=>'UserController@TableR']);
//	Sửa danh mục
Route::post('/update/{id}', ['as'=>'T', 'uses'=>'UserController@update'])->where(['id' => '[0-9]+']);
//	Xóa danh mục
Route::get('/delete/{id}', ['as'=>'D', 'uses'=>'UserController@destroy'])->where(['id' => '[0-9]+']);
