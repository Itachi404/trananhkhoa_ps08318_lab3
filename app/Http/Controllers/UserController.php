<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function TableR(){
        $form = DB::table('users') -> orderBy('id', 'ASC') -> get();
        return view('table',compact('form'));
    }

    public function create(Request $request)
    {
        $user = new User;
        $user->First_Name = $request->AA;
        $user->Last_Name = $request->BB;
        $user->email = $request->CC;
        $user->save();
		return redirect()->route('R');
    }


    public function update(Request $request, $id)
    {
         if ($Edit = $request->input('Edit')) {
            $form = DB::table('users') -> orderBy('id', 'ASC') -> get();
            return view('table',compact('form'));
        } else {
            $user = User::find($id);
            $user->First_Name = $request->A;
            $user->Last_Name = $request->B;
            $user->email = $request->C;
            $user->save();
            return redirect()->route('R');
        }
    }


    public function destroy($id)
    {
       	DB::table('users')->where('id', $id)->delete();
		return redirect()->route('R');
    }
}
