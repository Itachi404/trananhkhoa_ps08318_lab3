@extends('master')
@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Họ</th>
            <th scope="col">Tên</th>
            <th scope="col">Email</th>
            <th scope="col">Hành Động</th>
        </tr>
        </thead>
        <tbody>
            <form action="{{route('C')}}" method="POST">
                    @csrf()
                    <tr>
                        <th scope="row"></td>
                        <td><input type="text" name="AA"></td>
                        <td><input type="text" name="BB"></td>
                        <td><input type="text" name="CC"></td>
                        <td align="center">
                            <input name="Add" type="submit" value="Thêm mới"/>
                        </td>
                    </tr>
            </form>
        </tbody>
        <tbody>
            @foreach ($form as $ds)
                <form action="{{route('T', $ds->id)}}" method="post">
                    @csrf()
                    <tr>
                        @if(isset($_POST['Edit']) && $_POST['Edit'] == $ds->id)
                            <th scope="row">{{$ds->id}}</th>
                            <td><input type="text" value="{{$ds->First_Name}}" name="A"></td>
                            <td><input type="text" value="{{$ds->Last_Name}}" name="B"></td>
                            <td><input type="text" value="{{$ds->email}}" name="C"></td>
                            <td align="center">
                                <input name="Edit1" type="submit" value="Sửa mới"/>
                                <input type="button" value="Hủy bỏ" onClick="document.location.href='{{ url('./') }} '">
                            </td>
                        @else
                            <th scope="row">{{$ds->id}}</th>
                            <td>{{$ds->First_Name}}</td>
                            <td>{{$ds->Last_Name}}</td>
                            <td>{{$ds->email}}</td>
                            <td align="center">
                                <button name="Edit" type="submit" value="{{ $ds->id }}">Sửa</button>
                                <a href="{{route('D', $ds->id)}}" class="del">Xóa</a>
                            </td>
                        @endif
                    </tr>
                </form>
            @endforeach
        </tbody>
    </table>
@endsection
